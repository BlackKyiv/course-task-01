const dayjs = require('dayjs');
var utc = require('dayjs/plugin/utc');
dayjs.extend(utc);
var isLeapYear = require('dayjs/plugin/isLeapYear');
dayjs.extend(isLeapYear);

const express = require('express');
const app = express();
const port = 56201;

const bp = require('body-parser');
app.use(bp.text());

app.post('/square', (req, res) => {
  const number = +req.body;
  res.setHeader('content-type', 'application/json');
  res.send({
    number: number,
    square: Math.pow(number, 2),
  });
});

app.post('/reverse', (req, res) => {
  res.setHeader('content-type', 'text/plain');
  res.send(req.body.split('').reverse().join(''));
});

const dayOfWeek = [
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
];

app.get('/date/:year/:month/:day', (req, res) => {
  const day = dayjs.utc(`${req.params.year}-${req.params.month}-${req.params.day}`);
  res.setHeader('content-type', 'application/json');
  const now = new Date();
  res.send({
    weekDay: dayOfWeek[day.day()],
    isLeapYear: day.isLeapYear(),
    difference: Math.abs(dayjs.utc(`${now.getFullYear()}-${now.getMonth()+1}-${now.getDate()}`).diff(day, 'day')),
  });
});

app.listen(port, () => {
  console.log(`App listening on port ${port}`);
});
